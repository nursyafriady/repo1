<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php $title = " String" ?>;
    <title><?php echo $title ?></title>
</head>
<body>
    <!-- <h1> String </h1> -->
    <h1> Array </h1>
    <h2>TEs Repo</h2>
    <?php         
        // $kalimat = "Hello World";
        // $output = substr($kalimat, 6 , 5);
        // $output = str_replace('World', 'Dunia', $kalimat);
        // $output = strrev($kalimat);
        // $output = strtolower($kalimat);
        // $output = strtoupper($kalimat);
        // $output = strlen($kalimat);
        // $output = strpos($kalimat, 'World');
        // echo $output;

        $bio = array('yadi', 23, 'polisi');   
    ?>
    <ul>
        <li>Nama : <?php echo $bio[0] ?></li>
    </ul>

    <h1>Array Associative</h1>
    <?php 
        $bio2 = array(
            'nama' => 'adis', 
            'pekerjaan' => 'polisi'
        );
    ?>
    <ul>
        <li>Nama : <?php echo $bio2['nama'] ?></li>
    </ul>

    <h1>Array Multidimensional</h1>
    <?php 
        $bio3 = array(
            array('adis', 'polisi'), 
            array('izzan', 'tentara')
        );
    ?>
    <ul>
        <li>Nama1 : <?php echo $bio3[0][0] ?></li>
        <li>Kerja1 : <?php echo $bio3[0][1] ?></li>
        <li>Nama2 : <?php echo $bio3[1][0] ?></li>
        <li>Kerja2 : <?php echo $bio3[1][1] ?></li>
    </ul>
</body>
</html>